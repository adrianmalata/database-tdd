import model.Address;
import model.Person;
import model.Region;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import repository.PersonRepository;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class PersonRepositoryTest {

    private final Person personJane = new Person("Jane", "Doe", LocalDateTime.of(1960, 1, 30, 22, 14), ZoneId.of("-7"));
    private final Person personJohn = new Person("John", "Doe", LocalDateTime.of(1958, 12, 1, 11, 7), ZoneId.of("-5"));
    private Connection connection;
    private PersonRepository personRepository;

    @BeforeEach
    void setUp() throws SQLException {
        connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/people_db", "adrianmalata", "somepassword");
        connection.setAutoCommit(false);
        personRepository = new PersonRepository(connection);
    }

    @AfterEach
    void tearDown() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    @Test
    public void canSaveOnePerson() {
        Person savedPerson = personRepository.save(personJane, personRepository.getPERSON_ID());
        assertThat(savedPerson.getAddresses_id()).isGreaterThan(0);
    }

    @Test
    public void canSaveTwoPeople() {
        Person savedPerson1 = personRepository.save(personJane, personRepository.getPERSON_ID());
        Person savedPerson2 = personRepository.save(personJohn, personRepository.getPERSON_ID());
        assertThat(savedPerson1.getAddresses_id()).isNotEqualTo(savedPerson2.getAddresses_id());
    }

    @Test
    public void canSavePersonWithPrimaryAddress() {
        Address address = new Address("123 Beale St.", "Apt. 1A", "Wala Wala", "WA", "90210", "Fulton", Region.WEST, "United States");
        personJane.setPrimaryAddress(address);
        Person savedPerson = personRepository.save(personJane, personRepository.getPERSON_ID());
        if (savedPerson.getPrimaryAddress().isPresent()) {
            assertThat(savedPerson.getPrimaryAddress().get().getAddresses_id()).isGreaterThan(0);
        }
    }

    @Test
    public void canFindPersonById() {
        Person personTest = new Person("Test", "Testing", LocalDateTime.now(), ZoneId.of("+7"));
        personRepository.save(personTest, personRepository.getPERSON_ID());
        Person foundPerson = null;
        if (personRepository.findById(personTest.getAddresses_id()).isPresent()) {
            foundPerson = personRepository.findById(personTest.getAddresses_id()).get();
        }
        assertThat(foundPerson).isEqualTo(personTest);
    }

    @Test
    public void canFindPersonByIdWithPrimaryAddress() {
        Person personTest = new Person("Test", "Testing", LocalDateTime.now(), ZoneId.of("+7"));
        Address address = new Address("123 Beale St.", "Apt. 1A", "Wala Wala", "WA", "90210", "Fulton", Region.WEST, "United States");
        personTest.setPrimaryAddress(address);
        Person savedPerson = personRepository.save(personTest, personRepository.getPERSON_ID());
        Person foundPerson = null;
        if (personRepository.findById(savedPerson.getAddresses_id()).isPresent()) {
            foundPerson = personRepository.findById(savedPerson.getAddresses_id()).get();
        }
        assert foundPerson != null;
        if (foundPerson.getPrimaryAddress().isPresent()) {
            assertThat(foundPerson.getPrimaryAddress().get().getState()).isEqualTo("WA");
        }
    }

    @Test
    @Disabled
    public void canFindAllPeople() {
        List<Person> people = personRepository.findAll();
        Integer countDB = personRepository.count();
        int countList = people.size();
        int randomIndex = (int)(Math.random() * people.size());
        assertThat(people.get(randomIndex).getClass().getName()).isEqualTo(Person.class.getName());
        assertThat(countDB).isEqualTo(countList);
    }

    @Test
    public void personIdNotFound() {
        Optional<Person> foundPerson = personRepository.findById(-1);
        assertThat(foundPerson).isEmpty();
    }

    @Test
    public void canGetCount() {
        Integer startCount = personRepository.count();
        personRepository.save(personJane, personRepository.getPERSON_ID());
        personRepository.save(personJohn, personRepository.getPERSON_ID());
        Integer endCount = personRepository.count();
        assertThat(endCount).isEqualTo(startCount + 2);
    }

    @Test
    public void canDeletePerson() {
        Person personTest = new Person("Test", "Testing", LocalDateTime.now(), ZoneId.of("+7"));
        personRepository.save(personTest, personRepository.getPERSON_ID());
        Integer startCount = personRepository.count();
        personRepository.delete(personTest);
        Integer endCount = personRepository.count();
        assertThat(endCount).isEqualTo(startCount - 1);
    }

    @Test
    public void canDeleteMultiplePeople() {
        Person person1 = personRepository.save(personJane, personRepository.getPERSON_ID());
        Person person2 = personRepository.save(personJohn, personRepository.getPERSON_ID());
        Person personTest = new Person("Test", "Testing", LocalDateTime.now(), ZoneId.of("+7"));
        personRepository.save(personTest, personRepository.getPERSON_ID());
        Integer startCount = personRepository.count();
        personRepository.delete(person1, person2, personTest);
        Integer endCount = personRepository.count();
        assertThat(endCount).isEqualTo(startCount - 3);
    }

    @Test
    public void canUpdatePerson() {
        Person person = personRepository.save(personJane, personRepository.getPERSON_ID());
        Person person1 = null;
        if (personRepository.findById(person.getAddresses_id()).isPresent()) {
            person1 = personRepository.findById(person.getAddresses_id()).get();
        }
        person.setFirstName("Margaret");
        personRepository.update(person);
        Person person2 = null;
        if (personRepository.findById(person.getAddresses_id()).isPresent()) {
            person2 = personRepository.findById(person.getAddresses_id()).get();
        }
        System.out.println("Before change: " + person1);
        System.out.println("After change: " + person2);
        assertThat(person1).isNotNull();
        assertThat(person2).isNotNull();
        assertThat(person1.getAddresses_id()).isEqualTo(person2.getAddresses_id());
        assertThat(person1.getFirstName()).isNotEqualTo(person2.getFirstName());
    }

    @Test
    public void loadData() throws IOException {
        String pathToCsvFile = "/Users/adrianmalata/Downloads/Hr5m.csv";
        try (Stream<String> lines = Files.lines(Path.of(pathToCsvFile))) {
            lines
                .skip(101)
                .limit(100)
                .map(l -> l.split(",")).map(a -> {
                    LocalDate dob = LocalDate.parse(a[10], DateTimeFormatter.ofPattern("M/d/yyyy"));
                    LocalTime tob = LocalTime.parse(a[11], DateTimeFormatter.ofPattern("hh:mm:ss a"));
                    LocalDateTime dtob = LocalDateTime.of(dob, tob);
                    Person person = new Person(a[2], a[4], dtob, ZoneId.of("+0"));
                    person.setEmail(a[6]);
                    person.setSalary(new BigDecimal(a[25]));
                    return person;
                })
                .forEach(p -> personRepository.save(p, personRepository.getPERSON_ID()));
        }
    }
}
