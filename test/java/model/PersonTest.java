package model;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;

import static org.assertj.core.api.Assertions.assertThat;

class PersonTest {

    @Test
    public void testForEquality() {
        Person personJane1 = new Person("Jane", "Doe", LocalDateTime.of(1960, 1, 30, 22, 14), ZoneId.of("-7"));
        Person personJane2 = new Person("Jane", "Doe", LocalDateTime.of(1960, 1, 30, 22, 14), ZoneId.of("-7"));
        assertThat(personJane1).isEqualTo(personJane2);
    }

    @Test
    public void testForInequality() {
        Person personJane1 = new Person("Jane", "Doe", LocalDateTime.of(1960, 1, 30, 22, 14), ZoneId.of("-7"));
        Person personJane2 = new Person("Jane", "Does", LocalDateTime.of(1960, 1, 30, 22, 14), ZoneId.of("-7"));
        assertThat(personJane1).isNotEqualTo(personJane2);
    }

}