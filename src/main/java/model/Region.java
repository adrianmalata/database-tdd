package model;

public enum Region {
    NORTH,
    SOUTH,
    WEST,
    EAST,
    NORTHWEST,
    NORTHEAST,
    MIDWEST,
    MIDEAST,
    SOUTHWEST,
    SOUTHEAST
}
