package model;

import java.util.Objects;

public final class Address implements Entity {
    private Integer addresses_id;
    private String street;
    private String address2;
    private String city;
    private String state;
    private String postalCode;
    private String county;
    private Region region;
    private String country;

    public Address(Integer addresses_id, String street, String address2, String city, String state, String postalCode, String county, Region region, String country) {
        this.addresses_id = addresses_id;
        this.street = street;
        this.address2 = address2;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
        this.county = county;
        this.region = region;
        this.country = country;
    }

    public Address(String street, String address2, String city, String state, String postalCode, String county, Region region, String country) {
        this.street = street;
        this.address2 = address2;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
        this.county = county;
        this.region = region;
        this.country = country;
    }

    @Override
    public Integer getAddresses_id() {
        return addresses_id;
    }

    @Override
    public void setAddresses_id(Integer addresses_id) {
        this.addresses_id = addresses_id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (Address) obj;
        return Objects.equals(this.addresses_id, that.addresses_id) &&
                Objects.equals(this.street, that.street) &&
                Objects.equals(this.address2, that.address2) &&
                Objects.equals(this.city, that.city) &&
                Objects.equals(this.state, that.state) &&
                Objects.equals(this.postalCode, that.postalCode) &&
                Objects.equals(this.county, that.county) &&
                Objects.equals(this.region, that.region) &&
                Objects.equals(this.country, that.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(addresses_id, street, address2, city, state, postalCode, county, region, country);
    }

    @Override
    public String toString() {
        return "Address[" +
                "id=" + addresses_id + ", " +
                "street=" + street + ", " +
                "address2=" + address2 + ", " +
                "city=" + city + ", " +
                "state=" + state + ", " +
                "postalCode=" + postalCode + ", " +
                "county=" + county + ", " +
                "region=" + region + ", " +
                "country=" + country + ']';
    }

}
