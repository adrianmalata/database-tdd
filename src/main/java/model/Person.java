package model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Objects;
import java.util.Optional;

public class Person implements Entity {
    private Integer id;
    private String firstName;
    private String lastName;
    private final LocalDateTime DOB;
    private final ZoneId ZONE_ID;
    private String email;
    private BigDecimal salary;
    private Optional<Address> primaryAddress = Optional.empty();

    public Person(String firstName, String lastName, LocalDateTime dob, ZoneId zoneId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.DOB = dob;
        this.ZONE_ID = zoneId;
    }

    public Person(Integer id, String firstName, String lastName, LocalDateTime dob, ZoneId zoneId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.DOB = dob;
        this.ZONE_ID = zoneId;
    }

    @Override
    public void setAddresses_id(Integer addresses_id) {
        this.id = addresses_id;
    }

    @Override
    public Integer getAddresses_id() {
        return id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDateTime getDOB() {
        return DOB;
    }

    public ZoneId getZONE_ID() {
        return ZONE_ID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public void setPrimaryAddress(Address primaryAddress) {
        this.primaryAddress = Optional.ofNullable(primaryAddress);
    }

    public Optional<Address> getPrimaryAddress() {
        return primaryAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(id, person.id) && Objects.equals(firstName, person.firstName) && Objects.equals(lastName, person.lastName) && Objects.equals(DOB, person.DOB) && Objects.equals(ZONE_ID, person.ZONE_ID) && Objects.equals(email, person.email) && Objects.equals(salary, person.salary) && Objects.equals(primaryAddress, person.primaryAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, DOB, ZONE_ID, email, salary, primaryAddress);
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", DOB=" + DOB +
                ", ZONE_ID=" + ZONE_ID +
                ", email='" + email + '\'' +
                ", salary=" + salary +
                ", primaryAddress=" + primaryAddress +
                '}';
    }
}
