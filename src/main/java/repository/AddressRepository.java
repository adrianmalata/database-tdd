package repository;

import model.Address;
import model.Region;
import org.jooq.*;
import org.jooq.Record;

import java.sql.Connection;
import java.util.List;

import static org.jooq.impl.DSL.*;

public class AddressRepository extends CRUDRepository<Address> {

    protected final Table<Record> ADDRESSES = table("addresses");
    protected final Field<Integer> ADDRESSES_ID = field("addresses_id", Integer.class);
    protected final Field<String> STREET = field("street", String.class);
    protected final Field<String> ADDRESS2 = field("address2", String.class);
    protected final Field<String> CITY = field("city", String.class);
    protected final Field<String> STATE = field("state", String.class);
    protected final Field<String> POSTAL_CODE = field("postal_code", String.class);
    protected final Field<String> COUNTY = field("county", String.class);
    protected final Field<String> REGION = field("region", String.class);
    protected final Field<String> COUNTRY = field("country", String.class);

    public AddressRepository(Connection CONNECTION) {
        super(CONNECTION);
    }

    @Override
    protected InsertSetMoreStep<?> prepareInsert(Address address) {
        return JOOQ
                .insertInto(ADDRESSES)
                .set(STREET, address.getStreet())
                .set(ADDRESS2, address.getAddress2())
                .set(CITY, address.getCity())
                .set(STATE, address.getState())
                .set(POSTAL_CODE, address.getPostalCode())
                .set(COUNTY, address.getCounty())
                .set(REGION, address.getRegion().toString())
                .set(COUNTRY, address.getCountry());
    }

    @Override
    protected Result<Record> fetchForEntityAsResult(Address address) {
        return JOOQ
                .select()
                .from(ADDRESSES)
                .where(
                        STREET.eq(address.getStreet())
                                .and(POSTAL_CODE.eq(address.getPostalCode())))
                .fetch();
    }

    @Override
    protected Record fetchForEntityByIdAsRecord(Integer id) {
        return JOOQ
                .select()
                .from(ADDRESSES)
                .where(ADDRESSES_ID.eq(id))
                .fetchOne();
    }

    @Override
    protected Address extractEntityFromRecord(Record record) {
        if (record.get(ADDRESSES_ID) == null) return null;
        Integer recordId = record.get(ADDRESSES_ID);
        String recordStreet = record.get(STREET);
        String recordAddress2 = record.get(ADDRESS2);
        String recordCity = record.get(CITY);
        String recordState = record.get(STATE);
        String recordPostalCode = record.get(POSTAL_CODE);
        String recordCounty = record.get(COUNTY);
        Region recordRegion = Region.valueOf(record.get(REGION).toUpperCase());
        String recordCountry = record.get(COUNTRY);
        return new Address(recordId, recordStreet, recordAddress2, recordCity, recordState, recordPostalCode, recordCounty, recordRegion, recordCountry);
    }

    @Override
    protected List<Address> fetchForAllEntities() {
        return null;
    }

    @Override
    protected void updateEntityInDB(Address address) {

    }
}
