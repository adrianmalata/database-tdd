package repository;
import model.Address;
import model.Person;
import org.jooq.*;
import org.jooq.Record;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.jooq.impl.DSL.*;

public class PersonRepository extends CRUDRepository<Person> {

    private final AddressRepository ADDRESS_REPOSITORY;
    private final Table<Record> PEOPLE = table("people");
    private final Field<Integer> PERSON_ID = field("people_id", Integer.class);
    private final Field<String> FIRST_NAME = field("first_name", String.class);
    private final Field<String> LAST_NAME = field("last_name", String.class);
    private final Field<Timestamp> DOB = field("dob", Timestamp.class);
    private final Field<String> ZONE_ID = field("zone_id", String.class);
    private final Field<String> EMAIL = field("email", String.class);
    private final Field<BigDecimal> SALARY = field("salary", BigDecimal.class);
    private final Field<Integer> PRIMARY_ADDRESS = field("primary_address", Integer.class);

    public PersonRepository(Connection CONNECTION) {
        super(CONNECTION);
        this.ADDRESS_REPOSITORY = new AddressRepository(CONNECTION);
    }

    @Override
    protected InsertSetMoreStep<?> prepareInsert(Person person) {
        InsertSetMoreStep<?> insert = JOOQ
                .insertInto(PEOPLE)
                .set(FIRST_NAME, person.getFirstName())
                .set(LAST_NAME, person.getLastName())
                .set(DOB, Timestamp.valueOf(person.getDOB()))
                .set(ZONE_ID, person.getZONE_ID().toString())
                .set(EMAIL, person.getEmail())
                .set(SALARY, person.getSalary());

        Address savedAddress;
        Optional<Address> primaryAddress = person.getPrimaryAddress();
        if (primaryAddress.isPresent()) {
            savedAddress = ADDRESS_REPOSITORY.save(primaryAddress.get(), ADDRESS_REPOSITORY.ADDRESSES_ID);
            insert = insert.set(PRIMARY_ADDRESS, savedAddress.getAddresses_id());
        }
        return insert;
    }

    @Override
    protected Result<Record> fetchForEntityAsResult(Person entity) {
        return JOOQ
                .select()
                .from(PEOPLE)
                .where(
                        FIRST_NAME.eq(entity.getFirstName())
                                .and(LAST_NAME.eq(entity.getLastName()))
                )
                .fetch();
    }

    @Override
    protected Record fetchForEntityByIdAsRecord(Integer id) {
        return JOOQ
                .select()
                .from(PEOPLE)
                .leftOuterJoin(ADDRESS_REPOSITORY.ADDRESSES)
                .on(PRIMARY_ADDRESS.eq(ADDRESS_REPOSITORY.ADDRESSES_ID))
                .where(PERSON_ID.eq(id))
                .fetchOne();
    }

    @Override
    protected Person extractEntityFromRecord(Record record) {
        Integer recordId = record.getValue(PERSON_ID);
        String recordFirstName = record.getValue(FIRST_NAME);
        String recordLastName = record.getValue(LAST_NAME);
        LocalDateTime recordDob = record.getValue(DOB).toLocalDateTime();
        ZoneId recordZoneId = ZoneId.of(record.getValue(ZONE_ID));
        String recordEmail = record.getValue(EMAIL);
        BigDecimal recordSalary = record.getValue(SALARY);
        Address primaryAddress = ADDRESS_REPOSITORY.extractEntityFromRecord(record);
        Person entity = new Person(recordId, recordFirstName, recordLastName, recordDob, recordZoneId);
        entity.setEmail(recordEmail);
        entity.setSalary(recordSalary);
        entity.setPrimaryAddress(primaryAddress);
        return entity;
    }

    @Override
    protected List<Person> fetchForAllEntities() {
        return JOOQ
                .select()
                .from(PEOPLE)
                .fetch()
                .stream()
                .map(this::extractEntityFromRecord)
                .collect(Collectors.toList());
    }

    @Override
    protected void updateEntityInDB(Person entity) {
        JOOQ
            .update(PEOPLE)
            .set(FIRST_NAME, entity.getFirstName())
            .set(LAST_NAME, entity.getLastName())
            .set(DOB, Timestamp.valueOf(entity.getDOB()))
            .set(ZONE_ID, entity.getZONE_ID().toString())
            .execute();
    }

    public Integer count() {
        return super.count(PEOPLE);
    }

    public void delete(Person entity) {
        super.delete(entity, PEOPLE, PERSON_ID);
    }

    public void delete(Person... people) {
        super.delete(PEOPLE, PERSON_ID, people);
    }

    public Field<Integer> getPERSON_ID() {
        return PERSON_ID;
    }
}
