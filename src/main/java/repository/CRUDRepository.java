package repository;

import model.Entity;
import org.jooq.*;
import org.jooq.Record;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class CRUDRepository<T extends Entity> {
    protected final DSLContext JOOQ;

    public CRUDRepository(Connection CONNECTION)  {
        this.JOOQ = DSL.using(CONNECTION, SQLDialect.POSTGRES);
    }

    public T save(T entity, Field<Integer> idField) {
        Integer id = insertEntityIntoDB(entity, idField);
        entity.setAddresses_id(id);
        return entity;
    }

    public Optional<T> findById(Integer id) {
        Record foundEntityRecord = fetchForEntityByIdAsRecord(id);
        T foundEntity = null;
        if (foundEntityRecord != null) {
            foundEntity = extractEntityFromRecord(foundEntityRecord);
        }
        return Optional.ofNullable(foundEntity);
    }

    public List<T> findAll() {
        return fetchForAllEntities();
    }

    public void delete(T entity, Table<Record> table, Field<Integer> idField) {
        Integer recordsDeleted = deleteEntityFromDB(entity, table, idField);
        System.out.println("Records deleted: " + recordsDeleted);
    }

    @SafeVarargs
    public final void delete(Table<Record> table, Field<Integer> idField, T... entities) {
        Integer recordsDeleted = deleteEntitiesFromDB(table, idField, entities);
        System.out.println("Records deleted: " + recordsDeleted);
    }

    public void update(T entity) {
        updateEntityInDB(entity);
    }

    protected Integer deleteEntityFromDB(T entity, Table<Record> table, Field<Integer> idField) {
        return JOOQ
                .deleteFrom(table)
                .where(idField.eq(entity.getAddresses_id()))
                .execute();
    }

    @SafeVarargs
    protected final Integer deleteEntitiesFromDB(Table<Record> table, Field<Integer> idField, T... entities) {
        List<Integer> ids = Arrays.stream(entities).map(T::getAddresses_id).collect(Collectors.toList());
        return JOOQ
                .deleteFrom(table)
                .where(idField.in(ids))
                .execute();
    }

    protected Integer count(Table<Record> table) {
        return JOOQ.fetchCount(table);
    }

    protected Integer insertEntityIntoDB(T entity, Field<Integer> idField) {
        InsertSetMoreStep<?> insert = prepareInsert(entity);
        return Objects.requireNonNull(insert.returning(idField).fetchOne()).getValue(idField);
    }

    protected abstract InsertSetMoreStep<?> prepareInsert(T entity);

    protected abstract Result<Record> fetchForEntityAsResult(T entity);

    protected abstract Record fetchForEntityByIdAsRecord(Integer id);

    protected abstract T extractEntityFromRecord(Record record);

    protected abstract List<T> fetchForAllEntities();
    protected abstract void updateEntityInDB(T entity);
}
