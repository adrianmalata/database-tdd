## CRUD Database repository TDD

***

### Name

Simple CRUD Database repository done using TDD

### Description

This code is written using TDD approach. There are two tables: people and addresses.

You can:
* C-reate new person or new address
* R-ead a person or address
* U-pdate a person or address
* D-elete a person or address

Works with PostgreSQL.

### Libraries

* JUnit Jupiter 5.10.1 
* JDBC 42.7.0
* JOOQ 3.18.7

***